# SIRONA (Backend Code)

SIRONA adalah project IoT yang bertujuan untuk menyajikan data-data guna mempermudah operasinal PDAM.

## Table of Contents

1. Arcihtecture
2. Technology
3. How to Run
4. Environtment

## Architecture


## Technology

Teknologi yang digunakan untuk membangun backend dari project backend SIRONA adalah:
1. NodeJS
    * MarbleJS (https://docs.marblejs.com/)
    * Type-ORM (https://typeorm.io/)
2. PostgresSQL
3. InfluxDB

# How to Run

1. Open terminal
2. Move to `<project>/src`
3. Run with:
    - Development mode: `yarn start:dev` or `npm run start:dev`
    - Production mode: `yarn start` or `npm start`