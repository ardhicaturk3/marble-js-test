import { t } from "@marblejs/middleware-io";
import { optional } from "../../modules/optional.modules";

export const usersParamsDto = {
  params: t.type({
    id: t.string,
  })
}

export const usersQueryDto = {
  query: t.type({
    id: t.string,
    name: optional(t.string)
  })
}