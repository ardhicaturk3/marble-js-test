import { throwError, of } from "rxjs";

/**
 * Get user info by id
 * @param id user id
 */
export const getUserById = (id: string) =>
  id === '1'
  ? of({ id: '1', name: 'Test' })
  : throwError(new Error('User not found'));