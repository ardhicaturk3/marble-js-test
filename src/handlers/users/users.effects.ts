import { r, HttpError, HttpStatus, use, combineEffects, combineRoutes } from "@marblejs/core";
import { mapTo, mergeMap, catchError, map } from "rxjs/operators";
import { throwError } from "rxjs";
import { requestValidator$ } from "@marblejs/middleware-io";
import { pipe } from 'fp-ts/lib/pipeable';
import { usersParamsDto, usersQueryDto } from "./users.dto";
import { getUserById } from "./users.controllers";

// handle /users request
const getUsersList$ = r.pipe(
  r.matchPath('/'),
  r.matchType('GET'),
  r.useEffect(req$ => req$.pipe(
    mapTo({body: "Users test"})
  ))
)

// handle /users/query
const getUserInQuery$ = r.pipe(
  r.matchPath('/q'),
  r.matchType('GET'),
  r.useEffect(req$ => req$.pipe(
    use(requestValidator$(usersQueryDto)),
    mergeMap(req => pipe(
      getUserById(req.query?.id),
      catchError(() => throwError(
        new HttpError('user doesn`t exists', HttpStatus.NOT_FOUND)
      ))
    )),
    map(body => ({body})),
  ))
)

// handle /users/:id request
const getUserById$ = r.pipe(
  r.matchPath('/:id'),
  r.matchType('GET'),
  r.useEffect(req$ => req$.pipe(
    use(requestValidator$(usersParamsDto)),
    mergeMap(req => pipe(
      getUserById(req.params?.id),
      catchError(() => throwError(
        new HttpError('user doesn`t exists', HttpStatus.NOT_FOUND)
      ))
    )),
    map(body => ({body})),
  ))
)

export const users$ = combineRoutes('/users', [
  getUsersList$,
  getUserInQuery$,
  getUserById$,
]);