import { combineRoutes } from "@marblejs/core";
import { users$ } from "./users/users.effects";
import { device$ } from "./device/devices.effects";

export const api_v1$ = combineRoutes('/api/v1', [
  users$,
  device$,
])