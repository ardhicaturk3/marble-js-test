import { throwError } from "rxjs";
import { Devices } from "../../database/entities/devices/devices.entity";
import { getConnection } from "typeorm";


export const getByParam = async (params: string | undefined) => {
  const deviceRepository = getConnection().getRepository(Devices);
  const id = params ? parseInt(params) : undefined;
  return params ? await deviceRepository.findOne(id)
    : throwError(new Error('Device not found'));
}