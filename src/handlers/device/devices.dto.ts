import { t } from "@marblejs/middleware-io";
import { optional } from "../../modules/optional.modules";

export const devicesGetByParamsDto = {
  params: t.type({
    id: optional(t.string),
    HWID: optional(t.string),
  })
}

export const postNewDevices = {
  body: t.type({
    HWID: t.string,
    IMEI: optional(t.string),
    noHp: optional(t.string),
    label: optional(t.string),
    lat: optional(t.number),
    long: optional(t.number),
  })
}