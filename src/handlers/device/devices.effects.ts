import { r, HttpError, HttpStatus, use, combineEffects, combineRoutes, useContext } from "@marblejs/core";
import { mapTo, mergeMap, catchError, map } from "rxjs/operators";
import { throwError, from, Observable } from "rxjs";
import { requestValidator$ } from "@marblejs/middleware-io";
import { pipe } from 'fp-ts/lib/pipeable';
import { devicesGetByParamsDto } from "./devices.dto";
import { getByParam } from "./devices.controllers";
import { PostgresToken } from "../../database";


const getDeviceByParamId$ = r.pipe(
  r.matchPath('/:id'),
  r.matchType('GET'),
  r.useEffect(
    (req$) => {
      return req$.pipe(
        use(requestValidator$(devicesGetByParamsDto)),
        map(req => req.params.id),
        mergeMap(id => pipe(
          from(getByParam(id)),
          catchError(() => throwError(
            new HttpError('device doesn`t exist', HttpStatus.NOT_FOUND)
          ))
        )),
        map(body => ({ body }))
      )
    }
  )
)

export const device$ = combineRoutes('/devices', [
  getDeviceByParamId$
])