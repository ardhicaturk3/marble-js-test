import { createServer, bindTo } from '@marblejs/core';
import { IO } from 'fp-ts/lib/IO';
import { listener } from "./http.listener";
import * as dotenv from "dotenv";
import { PostgresToken, PostgresDI } from './database';

dotenv.config();

// defining server properties
const server = createServer({
  port: Number(process.env.PORT),
  hostname: '0.0.0.0',
  listener,
  dependencies: [
    bindTo(PostgresToken)(PostgresDI),
  ]
});

const main: IO<void> = async () =>{
  try{
    // server up
    await (await server)();

    // connect to postgres database
    // const pg = await pgConnection;
    
    // // when pg connection successful
    // if(pg){
    //   console.log("Success // Database Connection // Postgres");
    // }

  } catch (err){
    console.error(err);
  }
}


main();