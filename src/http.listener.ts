import { httpListener } from "@marblejs/core";
import { logger$ } from "@marblejs/middleware-logger";
import { bodyParser$ } from "@marblejs/middleware-body";
import { api_v1$ } from "./handlers/index.handlers";

const middlewares = [
  logger$(),
  bodyParser$(),
];

const effects = [
  api_v1$
];

export const listener = httpListener({
  middlewares,
  effects
});