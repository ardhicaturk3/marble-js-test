import { Entity, Column, PrimaryGeneratedColumn, CreateDateColumn, Generated, DeleteDateColumn, UpdateDateColumn, } from "typeorm";

@Entity()
export class Devices {
  // Create table entity / model
  @PrimaryGeneratedColumn()
  id: number | undefined;

  @Column({
    unique: true,
    nullable: false,
    type: "text",
  })
  HWID: string | undefined;

  @Column({
    unique: true,
    nullable: true,
    type: "text",
  })
  IMEI: string | undefined;

  @Column({
    name: "no_hp",
    nullable: true,
    unique: true,
    type: "text"
  })
  noHp: string | undefined;

  @Column({
    nullable: true,
    type: "float8"
  })
  lat: number | undefined;

  @Column({
    nullable: true,
    type: "float8"
  })
  long: number | undefined;

  @Column({
    name: "secret_key",
    nullable: false,
    type: "uuid",
  })
  @Generated("uuid")
  secretKey: string | undefined;

  @Column({
    type: "text",
    nullable: true,
  })
  label: string | undefined;

  @CreateDateColumn({
    name: "created_at",
    nullable: false,
    type: "timestamptz",
  })
  createdAt: Date | undefined;

  @UpdateDateColumn({
    name: "updated_at",
    nullable: true,
    type: "timestamptz",
  })
  updatedAt: Date | undefined;

  @DeleteDateColumn({
    name: "deleted_at",
    nullable: true,
    type: "timestamptz",
  })
  deletedAt: Date | undefined;

}