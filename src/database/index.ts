import "reflect-metadata";
import { createConnection } from "typeorm";
import * as dotenv from "dotenv";
import { createContextToken, reader } from "@marblejs/core";
import { pipe } from 'fp-ts/lib/pipeable';
import * as R from 'fp-ts/lib/Reader';

dotenv.config();

export const pgConnection = createConnection({
  name: 'default',
  type: "postgres",
  host: process.env.PG_HOST || "localhost",
  port: parseInt(process.env.PG_PORT || '5432'),
  username: process.env.PG_USER || "admin",
  password: process.env.PG_PASS || "admin",
  database: process.env.PG_DB || "pg",
  logging: ['error'],
  synchronize: true,
  entities: [
    __dirname + "/entities/**/*.ts"
  ],
})

export const PostgresToken = createContextToken<string>('Postgres');

export const PostgresDI = pipe(reader, R.map(async () => await pgConnection))