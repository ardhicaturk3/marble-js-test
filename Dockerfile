FROM node:13.8.0-alpine
WORKDIR /app

COPY ./src .
RUN apk add --no-cache bash && \
    npm install --only=prod --silent && \
    npm install --silent -g typescript ts-node

EXPOSE 3313
CMD ["npm", "start"]
